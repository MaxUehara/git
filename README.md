
Este es un archivo con cosos de prueba

Todos me reprochan lo que hice,
pero nadie dice una verdad aquí.
Piensan que la vida es eterna,
creen tener todo, pero no es así.
Y hace falta valor
para poder continuar
cargando este peso en soledad.
Mudo, ciego, necio, hombre tonto,
cuando abras los ojos muy tarde será.
Estoy esperando la revancha
no podrán vencerme soy la realidad.
La batalla será
digna como la verdad.
He luchado siempre hasta el final.
Como el trueno regresó,
vieja espada brilla al sol.
De otros tiempos va a venir
con su magia a combatir.
Todos me decían no se puede.
Estaba muy solo en una tempestad,
sumergido en lodo todo el tiempo,
pude ver sus ojos, pude ver el mal...
Ahora puedo gritar,
no lo quiero callar:
he luchado siempre hasta el final.
Luchar...
Hasta el final...
Hasta el final...
Luchando hasta el final...
Siempre hasta el final.

